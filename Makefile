
MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --warn-undefined-variables

.DELETE_ON_ERROR:


deploy.docker : App-Artifact-Repository-*.tar.gz
	mkdir --parents docker/ci
	cp --archive ci/bin docker/ci/
	cp --archive $< docker/
	cd docker ; PATH=$$PWD/ci/bin:$$PATH $(MAKE) deploy


