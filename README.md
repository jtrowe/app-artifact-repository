# App-Artifact-Repository

This is a simple binary artifact repository.
The primary use is going to be generic archive files.


## Getting Started

###  1. Start The Server

The server will start and you'll see it's log output.
Enter CTRL-c when you want it to stop.
This will be listening on http://localhost:3000/.

```
$ script/aar-server daemon
Name "DateTime::TO_JSON" used only once: possible typo at bin/server line 30.
[2022-06-07 16:03:34.67054] [3422] [debug] Creating storage directory "/home/jrowe/projects/App-Artifact-Repository/storage".
[2022-06-07 16:03:34.67095] [3422] [debug] storage_index:
{}
[2022-06-07 16:03:34.67174] [3422] [info] Listening at "http://*:3000"
Web application available at http://127.0.0.1:3000
```

### 2. Locate An Artifact To Upload


### 3. Generate A Metadata File

This will create a file name $file.meta.yml in the same
directory as $file.

```
$ bin/build-package-meta-file --name artifact_name --version any_version_string --tag latest --tag my_special_tag $file
2022/06/07 16:09:20 meta:
---
date: 1998-11-11T05:00:00+0000
file: example-1.0.tar.gz
meta_version: 1
name: example
tags:
  - latest
  - my_special_tag
version: 1.0
```


### 4. Upload The Files

It will upload the metadata file and the original file.

```
$ bin/upload $file.meta.yml`
file => example-1.0.tar.gz
RESPONSE:
201
Content-Length: 211
Content-Type: application/json;charset=UTF-8
Date: Tue, 07 Jun 2022 20:14:02 GMT
Server: Mojolicious (Perl)
X-Application-Name: App-Artifact-Repository
X-Application-Version: 0.1.0

{"file":{"filename":"example-1.0.tar.gz","size":217},"meta":{"date":"1998-11-11T05:00:00+0000","file":"example-1.0.tar.gz","meta_version":"1","name":"example","tags":["latest","my_special_tag"],"version":"1.0"}}
```


### 5. See The Uploaded Files

The files have now been uploaded to the 'storage' directory.

```
$ find storage -ls
 38232428      4 drwxrwxr-x   3 jrowe    jrowe        4096 Jun  7 16:14 storage/
 38150772      4 -rw-rw-r--   1 jrowe    jrowe         407 Jun  7 16:14 storage/index.json
 38150857      4 -rw-rw-r--   1 jrowe    jrowe         287 Jun  7 16:14 storage/meta.json
 38232430      4 drwxrwxr-x   3 jrowe    jrowe        4096 Jun  7 16:14 storage/example
 38232462      4 drwxrwxr-x   2 jrowe    jrowe        4096 Jun  7 16:14 storage/example/1.0
 38150770      4 -rw-rw-r--   1 jrowe    jrowe         139 Jun  7 16:14 storage/example/1.0/example-1.0.tar.gz.meta.yml
 38150726      4 -rw-rw-r--   1 jrowe    jrowe         217 Jun  7 16:14 storage/example/1.0/example-1.0.tar.gz
```


## Limitations

*   there is no access control
*   there is no authentication
*   there is no multi-threading; everything is done on the HTTP request
    thread
*   there is no monitoring
*   there is no configuration
*   there is no UI
*   logs are thrown away
*   known bugs in edge cases of version/tag indexing


## script/aar-server

`script/aar-server daemon`

*   Super basic right now.
*   Not threaded.
*   no way to fetch artifacts


## bin/upload

`bin/upload <meta_file>`

*   uploads the file referred to in the meta file
*   also uploads the meta file


## bin/fetch

`bin/fetch`

*   fetches an artifact

Examples:

```
bin/fetch --overwrite --debug example version=3.4 tag=foo1
bin/fetch --overwrite --debug example version=3.2 tag=foo1
bin/fetch --overwrite --debug example version=2.0
bin/fetch --overwrite --debug example tag=latest
```

## Dev Notes

prove:

```sh
rm -rf t/storage/ ; cover -delete ; HARNESS_PERL_SWITCHES=-MDevel::Cover AAR_CONFIG_FILE=aar-test.yml prove -Ilib -m -v t/C_Main.t 2>&1 | tee p.log ; cover
```

## CI

Using https://gitlab.com/jtrowe/perl-ci.


## Creating A Project Page For My Blog

```sh
dzil build
ci/bin/generate-project-page --meta App-Artifact-Repository-$version/META.yml > project.md
cp --archive project.md $hugo_project/content/projects/App-Artifact-Repository.md
```


