package App::Artifact::Repository::Server;


use App::Artifact::Repository;
use File::Basename;
use Mojo::Base 'Mojolicious', -signatures;
use Data::Printer;
use IO::All;
use Mojo::JSON qw( decode_json );


our $VERSION = '0.1.0';
my $name = 'App-Artifact-Repository';


has 'aar';
has 'build_info';


# This method will run once at server start
sub startup ($self) {

    $self->moniker('aar');

    my @config_params;

    if ( my $f = $ENV{AAR_CONFIG_FILE} ) {
        @config_params = ( { file => $f } );
    }

    my $config = $self->plugin('NotYAMLConfig', @config_params);

    # Configure the application
    $self->secrets($config->{secrets});

    my $aar = App::Artifact::Repository->new;
    $aar->config($config);
    $aar->log($self->log);
    $self->aar($aar);

    $self->hook(before_dispatch => sub ($c) {
        $c->log->debug("REQUEST.headers:\n" . $c->req->headers->to_string);
    });

    $self->hook(before_render => sub ($c, $args) {
        $c->res->headers->header('X-Application-Name' => $name);
        $c->res->headers->header('X-Application-Version' => $VERSION);
    });

    $self->hook(before_server_start => sub ($server, $app) {
        $aar->scan_storage($app);

        my $build_info_file = $self->find_package_related_file('build.json');

        my $b;
        $b < io->file($build_info_file);
        my $j = decode_json $b;
        $app->log->info("BUILD INFO ($build_info_file) :\n" . np($j));
        $app->build_info($j);
    });

    my $r = $self->routes;

    $r->get('/api/status')->to('Main#status');
    $r->get('/api/repo/:repo/index.json')->to('Main#repo_index');
    $r->get('/api/repo/:repo/meta.json')->to('Main#repo_meta_index');
    $r->get('/api/repo')->to('Main#repo_list');
    $r->get('/api/repo/:repo/file/*file/digest/:digest_type/:digest')
            ->to('Main#fetch');
    $r->get('/api/repo/:repo/file/*file')->to('Main#fetch');
    $r->get('/api/repo/:repo/file/*file/meta')->to('Main#fetch');
    $r->post('/api/repo/:repo/file/*file')->to('Main#store');

    # catchall route
    $r->any('/*p' => { p => '' } => sub ($c) {
        my $p = $c->param('p');
        my $result = {
            path    => "$p",
            message => 'Not Found',
        };
        $c->render(json => $result, status => 4040);
    });
}


sub find_package_related_file {
    my $self = shift;
    my $name = shift;

    my ( undef, $package_file ) = caller(0);
    $self->log->debug("find_file_by_path: pack_file=$package_file");

    my $package_dir = $package_file;
    $package_dir =~ s/\.pm$//;
    $self->log->debug("find_file_by_path: pack_dir=$package_dir");

    my $related_file = File::Spec->catfile($package_dir, 'build.json');
    $self->log->debug("find_file_by_path: pf_info=$related_file");

    return $related_file;
} # find_package_related_file


1;

