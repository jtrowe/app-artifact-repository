package App::Artifact::Repository::UrlBuilder;

use Role::Tiny;

use Mojo::URL;


# NB: Test::Mojo does not have ::log, so beware


sub build_fetch_artifact_by_digest_url {
    my $self        = shift;
    my %params      = @_;
    my $digest      = $params{digest};
    my $digest_type = $params{digest_type};

    my $url = $self->build_fetch_artifact_url(@_);

    return sprintf '%s/digest/%s/%s', $url, $digest_type, $digest;
} # build_fetch_artifact_by_digest_url


sub build_fetch_artifact_url {
    my $self         = shift;
    my %params       = @_;
    my $file         = $params{file};
    my $repo         = $params{repo};

    my $path = sprintf '/repo/%s/file/%s', $repo, $file;

    return $self->build_url(
        %params,
        path => $path,
    );
} # build_fetch_artifact_url


sub build_fetch_index_url {
    my $self         = shift;
    my %params       = @_;
    my $repo         = $params{repo};

    my $path = sprintf '/repo/%s/index.json', $repo;

    return $self->build_url(
        %params,
        path => $path,
    );
} # build_fetch_index_url


sub build_fetch_metadata_url {
    my $self   = shift;
    my %params = @_;

    $params{file} .= '/meta';

    return $self->build_fetch_artifact_url(%params);
} # build_fetch_metadata_url


sub build_list_repos_url {
    my $self = shift;

    return $self->build_url(
        @_,
        path => '/repo',
    );
} # build_list_repos_url


sub build_status_url {
    my $self = shift;

    return $self->build_url(
        @_,
        path => '/status',
    );
} # build_status_url


sub build_store_file_url {
    my $self = shift;

    return $self->build_fetch_artifact_url(@_);
} # build_store_file_url


sub build_url {
    my $self         = shift;
    my %params       = @_;
    my $path         = $params{path} // '/';
    my $port         = $params{port};
    my $q            = $params{query};
    my $relative_url = $params{relative_url};
    my $server       = $params{server} || 'localhost';

    my $base = 'http://' . $server;
    my $uri = Mojo::URL->new($base);
    if ( $port ) {
        $uri->port($port);
    }
    $uri->path(sprintf '/api%s', $path);

    $uri->query(%{ $q });

    unless ( $relative_url ) {
        return $uri;
    }

    return substr($uri, length($base));
} # build_url


1;

