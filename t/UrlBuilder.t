
use strict;
use warnings;


use lib 't/lib';

use Test::More;
use Test::Warnings;

use ImplUrlBuilder;


my $log_level = ( $ENV{HARNESS_IS_VERBOSE} || $ENV{TEST_VERBOSE} )
        ? 'debug' : 'info';
use Log::Any::Adapter 'TAP', filter => $log_level;


plan(tests => 10);


my $builder = new_ok('ImplUrlBuilder');


my $u;


subtest 'build_fetch_artifact_by_digest_url' => sub {
    plan(tests => 2);

    my %params = (
        digest      => '0000',
        digest_type => 'md3',
        file        => 'example.txt',
        repo        => 'demo',
    );

    my $expected_rel_url = '/api/repo/demo/file/example.txt/digest/md3/0000';

    $u = $builder->build_fetch_artifact_by_digest_url(
        relative_url => 1,
        %params,
    );
    is($u, $expected_rel_url,
            'build_url w/ relative_path is ok');

    $u = $builder->build_fetch_artifact_by_digest_url(
        relative_url => 0,
        %params,
    );
    is($u, "http://localhost$expected_rel_url",
            'build_url w/ absolute is ok');

};


subtest 'build_fetch_artifact_url' => sub {
    plan(tests => 2);

    my %params = (
        file => 'example.txt',
        repo => 'demo',
    );

    my $expected_rel_url = '/api/repo/demo/file/example.txt';

    $u = $builder->build_fetch_artifact_url(
        relative_url => 1,
        %params,
    );
    is($u, $expected_rel_url,
            'build_url w/ relative_path is ok');

    $u = $builder->build_fetch_artifact_url(
        relative_url => 0,
        %params,
    );
    is($u, "http://localhost$expected_rel_url",
            'build_url w/ absolute is ok');

};


subtest 'build_fetch_index_url' => sub {
    plan(tests => 2);

    my %params = (
        repo => 'demo',
    );

    my $expected_rel_url = '/api/repo/demo/index.json';

    $u = $builder->build_fetch_index_url(
        relative_url => 1,
        %params,
    );
    is($u, $expected_rel_url,
            'build_url w/ relative_path is ok');

    $u = $builder->build_fetch_index_url(
        relative_url => 0,
        %params,
    );
    is($u, "http://localhost$expected_rel_url",
            'build_url w/ absolute is ok');

};


subtest 'build_fetch_metadata_url' => sub {
    plan(tests => 3);

    my %params = (
        file => 'example.txt',
        repo => 'demo',
    );

    my $expected_rel_url        = '/api/repo/demo/file/example.txt/meta';
    my $expected_rel_url_with_d =
            '/api/repo/demo/file/example.txt/meta?debug=3';

    $u = $builder->build_fetch_metadata_url(
        relative_url => 1,
        %params,
    );
    is($u, $expected_rel_url,
            'build_url w/ relative_path is ok');

    $u = $builder->build_fetch_metadata_url(
        query        => {
            debug => 3,
        },
        relative_url => 1,
        %params,
    );
    is($u, $expected_rel_url_with_d,
            'build_url w/ relative_path is ok');

    $u = $builder->build_fetch_metadata_url(
        relative_url => 0,
        %params,
    );
    is($u, "http://localhost$expected_rel_url",
            'build_url w/ absolute is ok');

};


subtest 'build_list_repos_url' => sub {
    plan(tests => 2);

    my $expected_rel_url = '/api/repo';

    $u = $builder->build_list_repos_url(
        relative_url => 1,
    );
    is($u, $expected_rel_url,
            'build_url w/ relative_path is ok');

    $u = $builder->build_list_repos_url(
        relative_url => 0,
    );
    is($u, "http://localhost$expected_rel_url",
            'build_url w/ absolute is ok');

};


subtest 'build_status_url' => sub {
    plan(tests => 2);

    my $expected_rel_url = '/api/status';

    $u = $builder->build_status_url(
        relative_url => 1,
    );
    is($u, $expected_rel_url,
            'build_url w/ relative_path is ok');

    $u = $builder->build_status_url(
        relative_url => 0,
    );
    is($u, "http://localhost$expected_rel_url",
            'build_url w/ absolute is ok');

};


subtest 'build_store_file_url' => sub {
    plan(tests => 2);

    my %params = (
        file        => 'example.txt',
        repo        => 'demo',
    );

    my $expected_rel_url = '/api/repo/demo/file/example.txt';

    $u = $builder->build_store_file_url(
        relative_url => 1,
        %params,
    );
    is($u, $expected_rel_url,
            'build_url w/ relative_path is ok');

    $u = $builder->build_store_file_url(
        relative_url => 0,
        %params,
    );
    is($u, "http://localhost$expected_rel_url",
            'build_url w/ absolute is ok');

};


subtest 'build_url' => sub {
    plan(tests => 4);

    $u = $builder->build_url(
        relative_url => 1,
    );
    is($u, '/api/', 'build_url w/ relative_path is ok');


    $u = $builder->build_url(
        relative_url => 1,
        query        => {
            debug => 2,
        },
    );
    is($u, '/api/?debug=2', 'build_url w/ query is ok');


    $u = $builder->build_url(
        relative_url => 0,
    );
    is($u, 'http://localhost/api/', 'build_url w/ relative_path is ok');

    $u = $builder->build_url(
        port         => '3030',
        relative_url => 0,
        server       => 'example.com',
    );
    is($u, 'http://example.com:3030/api/', 'build_url w/ absolute is ok');


};


