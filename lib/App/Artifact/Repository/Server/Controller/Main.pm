package App::Artifact::Repository::Server::Controller::Main;


use Data::Printer;
use DateTime;
use DateTime::Format::Strptime;
use Digest::SHA;
use File::Basename;
use File::Find;
use File::Spec;
use IO::All;
use JSON;
use Mojo::Base 'Mojolicious::Controller', -signatures;
use YAML qw( Dump DumpFile Load );


sub fetch ($self) {
    my $digest      = $self->stash('digest');
    my $digest_type = $self->stash('digest_type');
    my $file        = $self->stash('file');
    my $repo_name   = $self->stash('repo');
    my $version     = $self->param('version');

    my %st = (
        digest      => $digest,
        digest_type => $digest_type,
        file        => $file,
        repo        => $repo_name,
        version     => ( $version // '' ),
    );
    $self->log->debug("::fetch : stash:\n" . np(%st));

    my $aar = $self->app->aar;

    unless ( $aar->repos->{$repo_name} ) {
        my $e = {
            message => 'Repo Not Found',
            repo    => $repo_name,
        };
        $self->render(json => $e, status => 404);

        return;
    }

    my $fetch_meta;
    if ( $file =~ s/\/meta$// ) {
        $fetch_meta = 1;
    }

    my $search_key;

    ( $search_key ) = $aar->repos->{$repo_name}->build_search_keys(
        digest      => $digest,
        digest_type => $digest_type,
        file        => $file,
        version     => $version,
    );

    $search_key //= $file;
    # FIXME Need to refactor how index key for fetch is built.
    #       Right now using same logic as for store;
    #       which really is quite different.
    #       store: build all the keys for the index
    #       fetch: choose the best key given the input params
    my $meta = $aar->repos->{$repo_name}->storage_index->{$search_key};
    $self->log->debug("search_key=" . $search_key);
    $self->log->debug("meta=" . ( $meta // '' ));

    unless ( $meta ) {
        my $e = {
            message => 'Artifact Not Found',

            digest      => $digest,
            digest_type => $digest_type,
            file        => $file,
            repo        => $repo_name,
            version     => $version,

            search_key  => $search_key,
        };
        $self->render(json => $e, status => 404);

        return;
    }

    $self->log->debug("meta:\n" . np($meta));

    my $meta_data = $aar->repos->{$repo_name}->meta_index->{$meta};

    if ( $fetch_meta ) {
        $self->render(json => $meta_data);
        return;
    }

    my $stored_file = $meta_data->{versioned_path};
    $self->log->debug('fetch: stored_file=' . $stored_file);

    unless ( -e $stored_file ) {
        my $e = {
            message => 'Artifact Not Found',

            digest      => $digest,
            digest_type => $digest_type,
            file        => $file,
            repo        => $repo_name,
            version     => ( $version // '' ),

            stored_file  => $stored_file,
        };
        $self->render(json => $e, status => 404);

        return;
    }

    my $base_name = basename($file);
    $self->res->headers->content_disposition(
            "attachment; filename=$base_name");
    $self->reply->file($stored_file);

} # fetch


sub index ($self) {
    my $storage_dir = $self->config->{storage_dir};
    my $meta_index_file = File::Spec->catfile($storage_dir, 'meta.json');

    $self->reply->file($meta_index_file);
} # index


sub load_meta_from_request {
    my $self       = shift;
    my $file_param = shift // 'file';
    my $meta_param = shift // 'meta';

    my $log = $self->log;
    my $aar = $self->app->aar;

    my $file = $self->param($file_param);
    my $meta = $self->param($meta_param);
    my $meta_json = $self->param('meta_json');

    my $meta_str;
    if ( $meta ) {
        $self->log->debug('Reading metadata from file upload.');

        $meta_str = $meta->slurp;
    }
    else {
        $self->log->debug('Reading metadata from form vars.');

        my $d = $self->req->param('date');
        my $v = $self->req->param('version');
        my $n = $self->req->param('name');
        my $mv = $self->req->param('meta_version');

        my $t = $self->req->param('tags') // '';
        my @z = split ',', $t;
        $self->log->debug('param.file=' . $file);
        my $filename = $file->filename;

        $meta_json = {
            date => $d,
            file => $filename,
            name => $n,
            path => $self->req->param('path'),
            version => $v,
            meta_version => $mv,
            tags => \@z,
        };
    }

    if ( $meta_json ) {
        $meta_str = Dump($meta_json);
    }
    $log->debug("meta:\n" . $meta_str);

    my $meta_data = Load($meta_str);

    $meta_data->{date} = $aar->parse_datetime($meta_data->{date});
    $meta_data->{meta_version} //= 3;
    delete $meta_data->{version};

    return $meta_data;
} # load_meta_from_request


sub repo_index {
    my $self      = shift;
$self->log->debug("\n\n\nALFA\n\n\n");
    my $repo_name = $self->stash('repo');

    my $aar = $self->app->aar;
    unless ( $aar->repos->{$repo_name} ) {
        my $e = {
            message => 'Repo Not Found',
            repo    => $repo_name,
        };
        $self->render(json => $e, status => 404);

        return;
    }

    my $storage_dir = $self->config->{storage_dir};
    my $index_file = File::Spec->catfile($storage_dir,
            $repo_name, 'index.json');

    $self->reply->file($index_file);
} # repo_index


sub repo_list {
    my $self = shift;

    $self->log->debug("REQUEST.headers:\n" . $self->req->headers->to_string);

    my $repos = $self->config->{repos};

    my %result = map { $_ => {} } keys %{ $repos };

    $self->render(json => \%result);
} # repo_list


sub repo_meta_index {
    my $self      = shift;
    my $repo_name = $self->stash('repo');

    my $aar = $self->app->aar;
    unless ( $aar->repos->{$repo_name} ) {
        my $e = {
            message => 'Repo Not Found',
            repo    => $repo_name,
        };
        $self->render(json => $e, status => 404);

        return;
    }

    my $storage_dir = $self->config->{storage_dir};
    my $index_file = File::Spec->catfile($storage_dir,
            $repo_name, 'meta.json');

    $self->reply->file($index_file);
} # repo_meta_index


sub status {
    my $self = shift;

    my $app = $self->app;

    my $result = {
        build_info => $app->build_info,
        status     => 'ok',
        version    => $app->VERSION,
    };

    $self->render(json => $result);
} # status


sub store {
    my $self = shift;

    $self->log->debug("REQUEST.headers:\n" . $self->req->headers->to_string);

    my $aar = $self->app->aar;

    my $artifact  = $self->param('artifact');
    my $file      = $self->param('file');
    my $repo_name = $self->param('repo');

    unless ( $aar->repos->{$repo_name} ) {
        my $e = {
            message => 'Repo Not Found',
            repo    => $repo_name,
        };
        $self->render(json => $e, status => 404);

        return;
    }

    my $meta_data = $self->load_meta_from_request('artifact');
    $self->log->debug("meta_data:\n" . np($meta_data));

#NB: overwrite metadata with real upload info
    $meta_data->{file} = $file;
    # DEPRECATED: name, path
    delete $meta_data->{name};
    delete $meta_data->{path};
    $self->log->debug("meta_data.2:\n" . np($meta_data));

    my @file_base = ( $self->config->{storage_dir}, $repo_name );
    my @meta_base = ( @file_base, 'meta' );


    my ( $file_param_name, $file_param_path ) = fileparse($file);

    my $digestor = Digest::SHA->new(256);
    my $real_artfile;
    {
        my $real_artdir = $file_param_path;
        $real_artfile = File::Spec->catfile(
                $real_artdir, $file_param_name);

        my $info = $aar->repos->{$repo_name}->store_file($artifact, $real_artfile);
        # NB : overwriting
        $real_artfile = $info->{versioned_path};

        $meta_data->{digest}         = $info->{digest};
        $meta_data->{versioned_path} = $info->{versioned_path};
        $meta_data->{version}        = $info->{version};
    }

    my $meta_key;
    my $real_metafile;
    {
        my $real_metafile_base = basename $real_artfile;
        $real_metafile_base .= '.json';
        my $real_metadir = File::Spec->catdir(@meta_base, $file_param_path);
        $self->app->aar->make_path($real_metadir);
        $real_metafile = File::Spec->catfile(
                $real_metadir, $real_metafile_base);

        $self->app->aar->save_meta_data($meta_data, $real_metafile);
        $meta_key = $aar->repos->{$repo_name}->import_meta_data($meta_data);
    }


    my $result = {
        %{ $meta_data },
    };

    if ( $self->param('debug') ) {
        my $debug_info = {
            real_metafile => $real_metafile,
            real_artfile => $real_artfile,
            meta_key => $meta_key,
        };
        $result->{_debug} = $debug_info;
    }

    # FIXME : This should probably return the versioned file.
    #         Probably ought to use hash.
    my $loc = sprintf '/api/repo/%s/file/%s', $repo_name, $file;

    $self->res->headers->header(Location => $loc);
    $self->render(json => $result, status => 201);
} # store


1;

