package App::Artifact::Repository::Client;

use Mojo::UserAgent -base;

use Role::Tiny::With;
with qw( App::Artifact::Repository::UrlBuilder );


use App::Artifact::Repository;
use Data::Printer;
use DateTime;
use Log::Any;
use YAML qw( Dump LoadFile );


has 'log' => sub {
    my $self = shift;
    return Log::Any->get_logger(category => ref($self)),
};


sub fetch {
    my $self     = shift;
    my %params   = @_;
    my $artifact = $params{artifact};
    my $debug    = $params{debug};
    my $file     = $params{file};
    my $meta     = $params{meta};
    my $port     = $params{port};
    my $repo     = $params{repo};
    my $server   = $params{server};

    my $builder = $meta ?
            'build_fetch_metadata_url' : 'build_fetch_artifact_url';
    my $url = $self->$builder(%params);

    $self->log->info("Fetching artifact from url $url");
    my $tx = $self->get($url);
    my $res = $tx->result;

    my $result = {
        is_success => $res->is_success,
        response   => $res,
    };

    if ( '200' eq $res->code ) {
        my $msg = "RESPONSE:\n";
        if ( $debug ) {
            $msg .= sprintf "%s\n%s\n",
                    $res->code, $res->headers->to_string;
        }
        if ( $debug && $meta ) {
            $msg .= sprintf "\n%s\n", $res->body;
        }

        $self->log->debug($msg);

        unless ( $meta ) {
            my $filename = $res->headers->header('Content-Disposition');
            $filename =~ s/.*filename=//;
            $result->{filename} = $filename;
        }
    }
    else {
        if ( $self->log->is_debug ) {
            my $m = sprintf "%s\n%s\n\n%s",
                    $res->code, $tx->res->headers->to_string;
            $self->log->error("RESPONSE:\n" . $m);
        }
    }

    return $result;
} # fetch


sub upload {
    my $self     = shift;
    my %params   = @_;
    my $artifact = $params{artifact};
    my $debug    = $params{debug};
    my $file     = $params{file};
    my $port     = $params{port};
    my $repo     = $params{repo};
    my $server   = $params{server};

    my $url = $self->build_store_file_url(%params);

    my $form = {
        artifact => { file => $artifact },
        file     => $file,
    };
    my $tx = $self->post($url => form => $form);
    $self->log->debug("REQUEST:\n" . $tx->req->headers->to_string . "\r\n");
    $self->log->debug("RESPONSE:\n" . $tx->res->to_string . "\r\n");

    if ( $tx->res->is_error ) {
        $self->log->error("ERROR:  Qutting.");
        exit 1;
    }
} # upload


1;
