use Mojo::Base -strict;

use Test::Deep;
use Test::More;
use Test::Mojo;

use App::Artifact::Repository::FileStore;
use Data::Printer;
use File::Basename;
use File::Path qw( make_path );
use File::Spec;
#use File::Temp qw( tempdir tempfile );
use IO::All;

my $log_level = ( $ENV{HARNESS_IS_VERBOSE} || $ENV{TEST_VERBOSE} )
        ? 'debug' : 'info';
use Log::Any::Adapter 'TAP', filter => $log_level;


plan(tests => 2);


my $tmp_dir = 't/tmp/test_files_src';
make_path($tmp_dir);
note("tmp_dir => $tmp_dir");


my $fs = new_ok(
    'App::Artifact::Repository::FileStore' => [
        dir => 't/tmp/files_t_storage',
    ],
);


subtest 'dev store_file' => sub {
    plan(tests => 3);

    my $f1v1 = create_test_file('one.txt');
    note("file1_v1=$f1v1");

    my $new_v1 = $fs->store_file($f1v1, 'example/one.txt');
    note("new_v1:\n" . np($new_v1));
    ok($new_v1->{versioned_path});

    my $f1v2 = create_test_file('one.txt', 2);
    note("file1_v2=$f1v2");
    my $new_v2 = $fs->store_file($f1v2, 'example/one.txt');
    note("new_v2:\n" . np($new_v2));
    isnt($new_v2->{versioned_path}, $new_v1->{versioned_path});

    # store same file again
    my $f1v2_again = create_test_file('one.txt', 2);
    my $new_v2_again = $fs->store_file($f1v2_again, 'example/one.txt');
    note("new_v2_again:\n" . np($new_v2));
    is($new_v2_again->{versioned_path}, $new_v2->{versioned_path});

};


sub create_test_file {
    my $name    = shift;
    my $version = shift || 1;

    my $new_file = File::Spec->catfile($tmp_dir, $name);
    note("create_test_file: new_file => $new_file");

    my $txt  = "version: $version\n";
    io($new_file)->print($txt);

    return $new_file;
}


