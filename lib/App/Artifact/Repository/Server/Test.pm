package App::Artifact::Repository::Server::Test;

use base 'Test::Builder::Module';

use Test::Mojo -base;

use Role::Tiny::With;
with qw( App::Artifact::Repository::UrlBuilder );


use Test::Deep;
use Test::More;

use Data::Printer filters => [{
    'DateTime' => sub { return '' . $_[0]; },
}];
use JSON;


my $json_pp = JSON->new->pretty;
my $re_json = qr/application\/json/i;


sub fetch_artifact_by_digest_ok {
    my $self = shift;

    $self->builder->subtest(
        'fetch artifact by digest ok',
        \&_fetch_artifact_by_digest_ok,,
        , $self,
        @_
    );

    return $self;
} # fetch_artifact_by_digest_ok


sub _fetch_artifact_by_digest_ok {
    my $self         = shift;
    my $test_params  = shift;
    my $content_type = $test_params->{expected}->{'Content-Type'};

    plan(tests => 3);

    $test_params->{relative_url} = 1;
    my $url = $self->build_fetch_artifact_by_digest_url(%{ $test_params });

    $self->get_ok($url => { Accept => '*/*' })
        ->status_is(200)
        ->header_is('Content-Type', $content_type);

    note("RESPONSE.headers:\n"
        . $self->tx->res->headers->to_string
    );

    return $self;
} # _fetch_artifact_by_digest_ok


sub fetch_artifact_missing_file {
    my $self = shift;

    $self->builder->subtest(
        'fetch artifact missing file',
        \&_fetch_artifact_missing_file,,
        , $self,
        @_
    );

    return $self;
} # fetch_artifact_missing_file


sub _fetch_artifact_missing_file {
    my $self         = shift;
    my $test_params  = shift;

    plan(tests => 4);

    $test_params->{relative_url} = 1;
    my $url = $self->build_fetch_artifact_url(%{ $test_params });

    my $form = {
        %{ $test_params->{metadata} },

        artifact => { file => $test_params->{artifact} },
    };

    $self->get_ok($url => { Accept => '*/*' })
        ->status_is(404)
        ->header_like('Content-Type', $re_json);

    my $body = $json_pp->decode($self->tx->res->body);
    note("RESPONSE:\n"
        . $self->tx->res->headers->to_string
        . "\r\n\r\n" . $json_pp->encode($body)
    );

    cmp_deeply(
        $body,
        {
            search_key => ignore(),

            digest      => $test_params->{digest},
            digest_type => $test_params->{digest_type},
            file        => $test_params->{file},
            repo        => $test_params->{repo},
            repo        => $test_params->{repo},
            version     => $test_params->{version},

            message => 'Artifact Not Found',
        },
        'Response body has expected content',
    );


    return $self;
} # _fetch_artifact_missing_file


sub fetch_artifact_ok {
    my $self = shift;

    $self->builder->subtest(
        'fetch artifact ok',
        \&_fetch_artifact_ok,,
        , $self,
        @_
    );

    return $self;
} # fetch_artifact_ok


sub _fetch_artifact_ok {
    my $self         = shift;
    my $test_params  = shift;
    my $content_type = $test_params->{expected}->{'Content-Type'};

    plan(tests => 3);

    $test_params->{relative_url} = 1;
    my $url = $self->build_fetch_artifact_url(%{ $test_params });

    my $form = {
        %{ $test_params->{metadata} },

        artifact => { file => $test_params->{artifact} },
    };

    $self->get_ok($url => { Accept => '*/*' })
        ->status_is(200)
        ->header_is('Content-Type', $content_type);

    note("RESPONSE.headers:\n"
        . $self->tx->res->headers->to_string
    );

    return $self;
} # _fetch_artifact_ok


sub fetch_index_ok {
    my $self = shift;

    $self->builder->subtest(
        'fetch index',
        \&_fetch_index_ok,
        , $self,
        @_
    );

    return $self;
} # fetch_index_ok


sub _fetch_index_ok {
    my $self        = shift;
    my $test_params = shift;

    plan(tests => 2);

    $test_params->{relative_url} = 1;
    my $url = $self->build_fetch_index_url(%{ $test_params });

    $self->get_ok($url => { Accept => 'application/json' })
        ->status_is(200);

    my $body = $json_pp->decode($self->tx->res->body);
    note("RESPONSE:\n"
        . $self->tx->res->headers->to_string
        . "\r\n\r\n" . $json_pp->encode($body)
    );

    return $self;
} # _fetch_index_ok


sub fetch_meta_index_ok {
    my $self = shift;

    $self->builder->subtest(
        'fetch meta_index',
        \&_fetch_meta_index_ok,
        , $self,
        @_
    );

    return $self;
} # fetch_meta_index_ok


sub _fetch_meta_index_ok {
    my $self        = shift;
    my $test_params = shift;

    plan(tests => 2);

    $test_params->{relative_url} = 1;
    my $url = $self->build_fetch_index_url(%{ $test_params });

    $self->get_ok($url => { Accept => 'application/json' })
        ->status_is(200);

    my $body = $json_pp->decode($self->tx->res->body);
    note("RESPONSE:\n"
        . $self->tx->res->headers->to_string
        . "\r\n\r\n" . $json_pp->encode($body)
    );

    return $self;
} # _fetch_meta_index_ok


sub fetch_metadata_missing_file {
    my $self = shift;

    $self->builder->subtest(
        'fetch metadata missing file',
        \&_fetch_metadata_missing_file,
        , $self,
        @_
    );

    return $self;
} # fetch_metadata_missing_file


sub _fetch_metadata_missing_file {
    my $self        = shift;
    my $test_params = shift;

    plan(tests => 4);

    $test_params->{relative_url} = 1;
    my $url = $self->build_fetch_metadata_url(%{ $test_params });

    my $form = {
        %{ $test_params->{metadata} },

        artifact => { file => $test_params->{artifact} },
    };

    $self->get_ok($url => { Accept => 'application/json' })
        ->status_is(200)
        ->header_like('Content-Type', $re_json);

    my $body = $json_pp->decode($self->tx->res->body);
    note("RESPONSE:\n"
        . $self->tx->res->headers->to_string
        . "\r\n\r\n" . $json_pp->encode($body)
    );

    cmp_deeply(
        $body,
        {
            versioned_path => ignore(),

            %{ $test_params->{metadata} },

            file   => $test_params->{file},
            digest => {
                sha256 => re('^[0-9a-f]{64}$'),
            },
        },
        'Response body has expected content',
    );

    return $self;
} # _fetch_metadata_missing_file


sub fetch_metadata_ok {
    my $self = shift;

    $self->builder->subtest(
        'fetch metadata missing file',
        \&_fetch_metadata_ok,
        , $self,
        @_
    );

    return $self;
} # fetch_metadata_ok


sub _fetch_metadata_ok {
    my $self        = shift;
    my $test_params = shift;

    plan(tests => 4);

    $test_params->{relative_url} = 1;
    my $url = $self->build_fetch_metadata_url(%{ $test_params });

    my $form = {
        %{ $test_params->{metadata} },

        artifact => { file => $test_params->{artifact} },
    };

    $self->get_ok($url => { Accept => 'application/json' })
        ->status_is(200)
        ->header_like('Content-Type', $re_json);

    my $body = $json_pp->decode($self->tx->res->body);
    note("RESPONSE:\n"
        . $self->tx->res->headers->to_string
        . "\r\n\r\n" . $json_pp->encode($body)
    );

    cmp_deeply(
        $body,
        {
            versioned_path => ignore(),

            %{ $test_params->{metadata} },

            file   => $test_params->{file},
            digest => {
                sha256 => re('^[0-9a-f]{64}$'),
            },
        },
        'Response body has expected content',
    );

    return $self;
} # _fetch_metadata_ok


sub get_404 {
    my $self = shift;

    $self->builder->subtest(
        'get 404',
        \&_get_404,
        , $self,
        @_
    );

    return $self;
} # get_404


sub _get_404 {
    my $self        = shift;
    my $test_params = shift;

    plan(tests => 4);

    $test_params->{relative_url} = 1;
    my $url = $self->build_url(%{ $test_params });

    $self->get_ok($url => { Accept => 'application/json' })
        ->status_is(404)
        ->header_like('Content-Type', $re_json);

    my $body = $json_pp->decode($self->tx->res->body);
    note("RESPONSE:\n"
        . $self->tx->res->headers->to_string
        . "\r\n\r\n" . $json_pp->encode($body)
    );

    cmp_deeply(
        $body,
        {
            path    => sprintf('api%s', $test_params->{path}),
            message => 'Not Found',
        },
        'Response body has expected content',
    );

    return $self;
} # _get_404


sub _has_expected_index_keys {
    my $self        = shift;
    my $test_params = shift;

    plan(tests => 2);

    my $repo_name = $test_params->{repo};

    my $sto_index = $self->app->aar->repos->{$repo_name}->storage_index;
    note("repo(demo).storage_index:\n" . np($sto_index));

    my $file_key = $test_params->{file};
    note("file_key=$file_key");
    my @storage_index_keys = keys %{ $sto_index };
    note("repo(demo) storage_index_keys:\n" . np(@storage_index_keys));
    my @file_keys = grep { m/^$file_key/ } @storage_index_keys;
    note("repo(demo) file_keys:\n" . np(@file_keys));

    my $expected_version = $test_params->{response_extra_fields}->{version};
    cmp_ok(scalar(@file_keys), '>=', 2,
            'Got minimum number of index keys for file');

    my @keys_with_hashes = grep { m/\{sha256\}/ } @file_keys;
    cmp_ok(scalar(@keys_with_hashes), '>=', 1,
            'Got minimum number of index keys with hashes for file');

    return $self;
} # _has_expected_index_keys


sub list_repos_ok {
    my $self = shift;

    $self->builder->subtest(
        'list_repos_ok',
        \&_list_repos_ok,
        , $self,
        @_
    );

    return $self;
} # list_repos_ok


sub _list_repos_ok {
    my $self        = shift;
    my $test_params = shift;

    plan(tests => 4);

    my $url = $self->build_list_repos_url(
        relative_url => 1,
    );

    $self->get_ok($url => { Accept => 'application/json' })
        ->status_is(200)
        ->header_like('Content-Type', $re_json);

    my $body = $json_pp->decode($self->tx->res->body);
    note("RESPONSE:\n"
        . $self->tx->res->headers->to_string
        . "\r\n\r\n" . $json_pp->encode($body)
    );

    cmp_deeply(
        $body,
        {
            map { $_ => {} } @{ $test_params->{repos} },
        },
        'Response body has expected content',
    );

    return $self;
} # _list_repos_ok


sub status_ok {
    my $self = shift;

    $self->builder->subtest(
        'status ok',
        \&_status_ok,
        $self,
        @_,
    );

    return $self;
} # status_ok


sub _status_ok {
    my $self        = shift;
    my $test_params = shift;

    plan(tests => 4);

    $test_params->{relative_url} = 1;
    my $url = $self->build_status_url(%{ $test_params });

    $self->get_ok($url)
        ->status_is(200)
        ->header_like('Content-Type', $re_json);

    my $body = $json_pp->decode($self->tx->res->body);
    note("RESPONSE:\n"
        . $self->tx->res->headers->to_string
        . "\r\n\r\n" . $json_pp->encode($body)
    );

    cmp_deeply(
        $body,
        {
            status     => 'ok',
            version    => ignore(),
            build_info => {
                build_id => ignore(),
                commit   => ignore(),
                date     => ignore(),
            }
        },
        'Response body has expected content',
    );

    return $self;
} # _status_ok


sub store_file_by_form_missing_repo {
    my $self = shift;

    $self->builder->subtest(
        'store file by form_missing repo',
        \&_store_file_by_form_missing_repo,
        $self,
        @_,
    );

    return $self;
} # store_file_by_form_missing_repo


sub _store_file_by_form_missing_repo {
    my $self        = shift;
    my $test_params = shift;

    plan(tests => 4);

    $test_params->{relative_url} = 1;
    my $url = $self->build_store_file_url(%{ $test_params });

    my %metadata = %{ $test_params->{metadata} };

    my $form = {
        %metadata,

        artifact => { file => $test_params->{artifact} },
    };

    my $file = sprintf '/api/repo/%s/file/%s',
            $test_params->{repo}, $test_params->{file};

    $self->post_ok($url => form => $form)
        ->status_is(404)
        ->header_like('Content-Type', $re_json);

    my $body = $json_pp->decode($self->tx->res->body);
    note("RESPONSE:\n"
        . $self->tx->res->headers->to_string
        . "\r\n\r\n" . $json_pp->encode($body)
    );

    cmp_deeply(
        $body,
        {
            message => 'Repo Not Found',
            repo    => $test_params->{repo},
        },
        'Response body has expected content',
    );

    return $self;
} # _store_file_by_form_missing_repo


sub store_file_by_form_ok {
    my $self = shift;

    $self->builder->subtest(
        'store file by form',
        \&_store_file_by_form_ok,
        $self,
        @_,
    );

    return $self;
} # store_file_by_form_ok


sub _store_file_by_form_ok {
    my $self        = shift;
    my $test_params = shift;

    plan(tests => 6);

    $test_params->{relative_url} = 1;
    my $url = $self->build_store_file_url(%{ $test_params });

    my %metadata = %{ $test_params->{metadata} };
#    $metadata{tags} = join ',', @{ $metadata{tags} };

    my $form = {
        %metadata,

        artifact => { file => $test_params->{artifact} },
    };

    my $file = sprintf '/api/repo/%s/file/%s',
            $test_params->{repo}, $test_params->{file};

    $self->post_ok($url => form => $form)
        ->status_is(201)
        ->header_like('Content-Type', $re_json)
        ->header_like('Location', qr/$file/);

    my $body = $json_pp->decode($self->tx->res->body);
    note("RESPONSE:\n"
        . $self->tx->res->headers->to_string
        . "\r\n\r\n" . $json_pp->encode($body)
    );

    cmp_deeply(
        $body,
        {
            _debug => ignore(),

            versioned_path => ignore(),

            %{ $test_params->{metadata} },
            %{ $test_params->{response_extra_fields} // {} },

            file   => $test_params->{file},
            digest => {
                sha256 => re('^[0-9a-f]{64}$'),
            },
        },
        'Response body has expected content',
    );

    $self->builder->subtest(
        'has expected index keys',
        \&_has_expected_index_keys,
        $self,
        $test_params,
    );

    return $self;
} # _store_file_by_form_ok


1;
