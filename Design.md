# Design

*   Be able to pull down a file by name and sha.
    This insures that you can reliably fetch a specific known
    version of a file.
    *   /api/repo/:repo/file/:path/sha256/:digest
*   maybe add client config with profiles which has predefined
    path mappings into the server
    *   ie: project-build, project-release, doc-latest, metadata

