#!/usr/bin/env perl

use strict;
use warnings;

use Cwd;
use DateTime;
use DateTime::Format::Strptime;
use Digest::MD5;
use File::Basename;
use IO::All;
use Log::Log4perl qw( :easy get_logger );
use Getopt::Long;
use YAML qw( Dump );

my $dt_format = DateTime::Format::Strptime->new(
    pattern => '%FT%T%z',
);

my %opt = (
    overwrite => 0,
    path      => '',
    tag       => [],
);
GetOptions(
    'force|f'     => \$opt{overwrite},
    'name|n=s'    => \$opt{name},
    'overwrite'   => \$opt{overwrite},
    'path|p=s'    => \$opt{path},
    'tag|t=s@'    => \$opt{tag},
    'version|v=s' => \$opt{version},
);

Log::Log4perl->easy_init($DEBUG);
my $log = get_logger('build-package-meta-file');


my $base_file = shift @ARGV || do {
    $log->error('Argument <base_file> is missing.  Quitting.');
    exit 1;
};

my $filename = basename($base_file);
unless ( $opt{name } ) {
    $opt{name} = $filename;
}

#my $digestor = Digest::MD5->new;
#my $fh;
#unless ( open($fh, '<', $base_file) ) {
#    $log->error(
#            sprintf 'Unable to read file %s: %s', $base_file, $!);
#}
#
#$digestor->addfile($fh);
#close $fh;

my @file_stat = stat $base_file ;
# TODO: double check utc
my $dt = DateTime->from_epoch(epoch => $file_stat[9]);
my $meta_version = 2;
my %meta = (
    date         => $dt_format->format_datetime($dt),
    file         => $filename,
    #md5          => $digestor->hexdigest,
    meta_version => $meta_version,
    name         => '' . $opt{name},
    path         => $opt{path},
    tags         => $opt{tag},
    version      => $opt{version},
);


my $meta_str = Dump(\%meta);
$log->info("meta:\n" . $meta_str);

my $meta_file = $base_file . '.meta.yml';

my $write_file = 1;
if ( -e $meta_file ) {
    my $msg = sprintf 'File "%s" already exists.  ', $meta_file;

    if ( $write_file = $opt{overwrite} ) {
        $msg .= 'Overwriting file.';
    }
    else {
        $msg .= 'Not overwriting file without --overwrite flag.';
    }

    $log->warn($msg);
}

if ( $write_file ) {
    io($meta_file)->print($meta_str);
}

