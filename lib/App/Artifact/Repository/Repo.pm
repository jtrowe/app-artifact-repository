package App::Artifact::Repository::Repo;

use Mojo::Base -base;


use App::Artifact::Repository::FileStore;
use Data::Printer filters => [{
    'DateTime' => sub { return '' . $_[0]; },
}];
use DateTime;
use DateTime::Format::Strptime;
use File::Find;
use File::Path qw();
use File::Spec;
use File::Temp qw( tempfile );
use IO::All;
use JSON;
use Log::Any;


has 'dir';
has 'dt_format';
has 'files';
has 'json_formatter';
has 'log' => sub {
    my $self = shift;
    return Log::Any->get_logger(category => ref($self)),
};
has 'name';
has 'meta_index' => sub { return {} };
has 'storage_index' => sub { return {}; };


sub build_index_keys_with_digest {
    my $self     = shift;
    my $meta     = shift;
    my $meta_key = shift;

    my @index_keys;

    foreach my $t ( keys %{ $meta->{digest} // {} } ) {
        my $d = $meta->{digest}->{$t};
        push @index_keys, sprintf('%s|{%s}%s', $meta_key, $t, $d);
    }

    return @index_keys;
} # build_index_keys_with_digest


sub build_search_keys {
    my $self        = shift;
    my %params      = @_;
    my $digest      = $params{digest};
    my $digest_type = $params{digest_type};
    my $file        = $params{file};
    my $version     = $params{version};

    $self->log->debug("::build_search_keys : params:\n" . np(%params));

    my @search_keys;

    unless ( $version ) {
        my $latest_version = 0;

        # TODO Add some other info to keep track of latest version.
        #      For now, just be dumb and simple.
        foreach my $key ( keys %{ $self->storage_index } ) {
            $self->log->debug("build_search_keys: key=$key");
            if ( $key =~ m/$file\|(\d+)$/ ) {
                my $this_version = $1;
                if ( $latest_version < $this_version ) {
                    $latest_version = $this_version;
                }
            }
        }

        if ( $latest_version ) {
            $version = $latest_version;
        }
    }

    if ( $version ) {
        push @search_keys, "$file|$version";
    }

    return @search_keys;
} # build_search_keys


sub import_meta_data {
    my $self  = shift;
    my $meta  = shift;

    $self->log->debug("import_meta_data: meta:\n" . np($meta));

    # FIXME : can probably refactor how keys are constructed
    #         for easy of reading
    my @meta_key_seg       = $self->_build_index_path_segments($meta);
    my $meta_key           = join '/', @meta_key_seg;
    my $versioned_meta_key = join '|', $meta_key, $meta->{version};
    $self->log->debug('meta_key=' . $meta_key);
    $self->log->debug('versioned_meta_key=' . $versioned_meta_key);

    my @index_keys = $self->_build_index_keys($meta, $meta_key);

    $self->log->debug("import_meta_data: meta_key: "
            . "$meta_key ; keys:\n" . np(@index_keys));

    foreach my $i_key ( @index_keys ) {
        $self->log->debug("import_meta_data: meta_key: "
                . "index key $i_key points to $meta_key");
        $self->storage_index->{$i_key} = $versioned_meta_key;
    }

    # FIXME DRY 1
    unless ( $self->files ) {
        $self->make_path($self->dir);
        $self->log->debug('Initializing FileStore object.');
        $self->files(App::Artifact::Repository::FileStore->new(
            dir => $self->dir,
            log => $self->log,
        ));
    }

    $meta->{versioned_path} = $self->files->get_versioned_file(
        file    => $meta->{file},
        version => $meta->{version},
    );
    $self->meta_index->{$versioned_meta_key} = $meta;

    $self->save_storage_index;
    $self->save_meta_index;

    return $meta_key;
} # import_meta_data


sub import_meta_file {
    my $self = shift;
    my $file = shift;

    $self->log->debug('importing meta file ' . $file);

    my $meta = $self->load_meta_file($file);

    return $self->import_meta_data($meta);
} # import_meta_file


sub is_index_key_newer {
    my $self     = shift;
    my $key      = shift;
    my $new_meta = shift;

    my $file = $self->storage_index->{$key};
    unless ( $file ) {
        return 1;
    }

    my $meta = $self->meta_index->{$file};
    unless ( $meta ) {
        return 1;
    }

    my $json = $self->json_formatter->encode($meta);
    my $new_json = $self->json_formatter->encode($new_meta);
#    $app->log->debug(sprintf
#            "is_index_key_newer [%s] : meta:\n%s:\n new_meta:\n%s",
#            $key, $json, $new_json);

#    $app->log->debug(sprintf
#            'is_index_key_newer [%s] : meta.date=%s ; new_meta.date=%s',
#            $key, '' . $meta->{date}, '' . $new_meta->{date});

    my $is_newer = $new_meta->{date} > $meta->{date};
    $self->log->debug(sprintf 'is_index_key_newer is_newer=%d', $is_newer);

    return $is_newer;
} # is_index_key_newer


sub load_meta_file {
    my $self = shift;
    my $file = shift;

    my $meta;

    if ( $file =~ m/.ya?ml$/ ) {
        $meta = LoadFile($file);
    }
    elsif ( $file =~ m/.json$/ ) {
        my $str = io($file)->all;
        $meta = $self->json_formatter->decode($str);
    }
    else {
        die "unknown meta file type named '$file'\n";
    }

    $meta->{date} = $self->parse_datetime($meta->{date});

    $meta->{_filename} = $file;

    return $meta;
} # load_meta_file


#FIXME DRY ::Repository
sub make_path {
    my $self = shift;
    my $dir  = shift;

    my $errors;

    $self->log->debug(sprintf 'Creating directory "%s".', $dir);
    File::Path::make_path($dir, { error => $errors });
    $self->log->debug("errors:\n" . np($errors)) if $errors;
} # make_path


# FIXME : DRY ::Repository
sub parse_datetime {
    my $self  = shift;
    my $input = shift;

    my $dt = $input ? $self->dt_format->parse_datetime($input) : DateTime->now;
    if ( $dt ) {
        $dt->set_formatter($self->dt_format);
    }

    return $dt;
} # parse_datetime


sub save_meta_index {
    my $self = shift;

    $self->make_path($self->dir);
    my $file = File::Spec->catfile($self->dir, 'meta.json');
    $self->log->info(sprintf 'Saving meta_index to %s.', $file);
    io($file)->print($self->json_formatter->encode($self->meta_index));
} # save_meta_index


sub save_storage_index {
    my $self = shift;

    $self->make_path($self->dir);
    my $file = File::Spec->catfile($self->dir, 'index.json');
    $self->log->info(sprintf 'Saving storage_index to %s.', $file);
    io($file)->print($self->json_formatter->encode($self->storage_index));
} # save_storage_index


sub scan_storage_repo {
    my $self = shift;

    $self->log->info(sprintf 'Scanning storage repo %s directory %s.',
            $self->name, $self->dir);

    my $meta_dir = File::Spec->catdir($self->dir, 'meta');
    $self->make_path($meta_dir);

    find(
        {
            no_chdir => 1,
            wanted   => sub {
                if ( m/\.json$/ ) {
                    $self->import_meta_file($_);
                }
            },
        },
        $meta_dir,,
    );

    $self->log->debug("storage_index:\n" . np($self->storage_index));

    $self->save_meta_index;
    $self->save_storage_index;

} # scan_storage_repo


sub store_file {
    my $self   = shift;
    my $upload = shift;
    my $new    = shift;

    # FIXME DRY 1
    unless ( $self->files ) {
        $self->make_path($self->dir);
        $self->log->debug('Initializing FileStore object.');
        $self->files(App::Artifact::Repository::FileStore->new(
            dir => $self->dir,
            log => $self->log,
        ));
    }

    # FIXME : any way to avoid a double move?

    my $temp_dir = File::Spec->catdir($self->dir, '.tmp');
    $self->make_path($temp_dir);
    my ( undef, $real_file ) = tempfile(DIR => $temp_dir);
    $upload->move_to($real_file);

    return $self->files->store_file($real_file, $new);
} # store_file


sub _build_index_keys {
    my $self                = shift;
    my ( $meta, $meta_key ) = @_;

    my @base_keys = ( $meta_key );
    if ( my $v = $meta->{version} ) {
        push @base_keys, join('|', $base_keys[0], $v);
    }

    my @digest_keys = $self->build_index_keys_with_digest(@_);

    my @tags = @{ $meta->{tags} //= [] };

    my @tagged_keys;
    foreach my $tag ( @tags ) {
        foreach my $key ( @base_keys ) {
            my $new_key = join('|', $key, $tag);
            if ( $self->is_index_key_newer($new_key, $meta) ) {
                push @tagged_keys, $new_key;
            }
        }
    }

    return ( @base_keys, @digest_keys, @tagged_keys );
} # _build_index_keys


sub _build_index_path_segments {
    my $self = shift;
    my $meta = shift;

    my @parts = ();
    # DEPRECATED: name
    if ( my $n = $meta->{name} ) {
        push @parts, $n;
    }
    # DEPRECATED: path
    if ( my $p = $meta->{path} ) {
        push @parts, $p;
    }
    push @parts, $meta->{file};

    return @parts;
} # _build_index_path_segments


1;
