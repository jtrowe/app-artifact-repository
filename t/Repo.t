use Mojo::Base -strict;

use Test::Deep;
use Test::More;

use App::Artifact::Repository::Repo;
use Data::Printer;


my $log_level = ( $ENV{HARNESS_IS_VERBOSE} || $ENV{TEST_VERBOSE} )
        ? 'debug' : 'info';
use Log::Any::Adapter 'TAP', filter => $log_level;


plan(tests => 6);


my $r = new_ok('App::Artifact::Repository::Repo');


subtest 'build_index_keys' => sub {
    plan(tests => 1);

    my $meta_key = 'example/file.txt';
    my $version = 3;
    my $meta = {
        file        => $meta_key,
        version     => $version,
    };

    my @expected = (
         $meta_key,
         "$meta_key|$version",
    );
    my @got = $r->_build_index_keys($meta, $meta_key);

    note("got:\n" . np(@got));
    cmp_bag(\@got, \@expected, 'Got expected index keys');

};


subtest 'build_index_keys w/ digest' => sub {
    plan(tests => 1);

    my $meta_key = 'example/file.txt';

    my $digest_type = 'foo16';
    my $digest = 'gggggggg';
    my $version = 3;
    my $meta = {
        digest => {
            $digest_type => $digest,
        },
        file        => $meta_key,
        version     => $version,
    };

    my @expected = (
         $meta_key,
         "$meta_key|$version",
         "$meta_key|{$digest_type}$digest",
    );
    my @got = $r->_build_index_keys($meta, $meta_key);

    note("got:\n" . np(@got));
    cmp_bag(\@got, \@expected, 'Got expected index keys');

};


subtest 'build_index_keys w/ tags' => sub {
    plan(tests => 1);

    my $meta_key = 'example/file.txt';
    my $digest_type = 'foo16';
    my $digest = 'gggggggg';
    my @tags = qw( alfa bravo );
    my $version = 3;
    my $meta = {
        digest => {
            $digest_type => $digest,
        },
        file        => $meta_key,
        tags        => \@tags,
        version     => $version,
    };

    my @got = $r->_build_index_keys($meta, $meta_key);

    my @expected = (
         $meta_key,
         "$meta_key|$tags[0]",
         "$meta_key|$tags[1]",
         "$meta_key|$version",
         "$meta_key|$version|$tags[0]",
         "$meta_key|$version|$tags[1]",
         "$meta_key|{$digest_type}$digest",
    );

    note("got:\n" . np(@got));
    cmp_bag(\@got, \@expected, 'Got expected index keys');

};


subtest 'build_search_keys w/ version' => sub {
    plan(tests => 1);

    my $file        = 'example/file.txt';
    my $version     = 3;

    my @got = $r->build_search_keys(
        file        => $file,
        version     => $version,
    );

    my @expected = (
         "$file|$version",
    );

    note("got:\n" . np(@got));
    cmp_bag(\@got, \@expected, 'Got expected search keys');

};


subtest 'build_search_keys w/ only file' => sub {
    plan(tests => 1);

    my $file            = 'example/file.txt';
    my $latest_version = 3;

    my @many_storage_keys = (
        'foo',
        'bar',
    );

    for ( my $i = 1; $i <= $latest_version; $i++ ) {
        push @many_storage_keys, "$file|$i";
    }

    my $repo = App::Artifact::Repository::Repo->new(
        storage_index => {
            map { $_ => 0 } @many_storage_keys,
        },
    );

    my @got = $repo->build_search_keys(
        file => $file,
    );

    my @expected = (
         "$file|$latest_version",
    );

    note("got:\n" . np(@got));
    cmp_bag(\@got, \@expected, 'Got expected search keys');

};


