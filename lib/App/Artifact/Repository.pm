package App::Artifact::Repository;

# ABSTRACT: An application which stores generic artifacts


use App::Artifact::Repository::Repo;
use Data::Printer;
use DateTime;
use DateTime::Format::Strptime;
use File::Basename;
use File::Path qw();
use File::Spec;
use IO::All;
use JSON;
use Mojo::Base -base;
use YAML qw( Load LoadFile);


my %meta_index;
my %storage_index;

*DateTime::TO_JSON = sub {
    my $self = shift;
    return '' . $self;
};


has 'config';
has 'dt_format' => sub {
    return DateTime::Format::Strptime->new(
        pattern => '%FT%T%z',
    );
};
has 'json_formatter' => sub {
    return JSON->new->convert_blessed->pretty;
};
has 'log';
has 'repos' => sub { return {} };


#FIXME DRY ::Repository::Repo
sub make_path {
    my $self = shift;
    my $dir  = shift;

    my $errors;

    $self->log->debug(sprintf 'Creating directory "%s".', $dir);
    File::Path::make_path($dir, { error => $errors });
    $self->log->debug("errors:\n" . np($errors)) if $errors;
} # make_path


# FIXME : DRY ::Repo
sub parse_datetime {
    my $self  = shift;
    my $input = shift;

    my $dt = $input ? $self->dt_format->parse_datetime($input) : DateTime->now;
    if ( $dt ) {
        $dt->set_formatter($self->dt_format);
    }

    return $dt;
} # parse_datetime


sub save_meta_data {
    my $self  = shift;
    my $meta  = shift;
    my $file  = shift;

    io($file)->print($self->json_formatter->encode($meta));

} # save_meta_data


sub scan_storage {
    my $self = shift;

    my $storage_dir = $self->config->{storage_dir};
    $self->log->info("Scanning storage directory $storage_dir.");

    my $repos = $self->config->{repos};
    $self->log->debug("repos:\n" . np($repos));

    foreach my $name ( keys %{ $repos } ) {
        my $repo = App::Artifact::Repository::Repo->new(
            dir  => File::Spec->catdir($storage_dir, $name),
            dt_format => $self->dt_format,
            json_formatter => $self->json_formatter,
            log  => $self->log,
            name => $name,
        );

        $repo->scan_storage_repo;

        $self->repos->{$name} = $repo;
    }

} # scan_storage


1;

