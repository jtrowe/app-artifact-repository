package App::Artifact::Repository::FileStore;


use Moo;

use Data::Printer;
use Digest::SHA;
use File::Basename;
use File::Spec;
use File::Path qw( make_path );
use Log::Any;


has 'dir' => (
    is => 'ro',
);


has 'log' => (
    is => 'ro',
    default => sub {
        return Log::Any->get_logger;
    },
);


my %file_version;


sub calc_digest {
    my $self = shift;
    my $file = shift;

    $self->log->debug("calc_digest: file=$file");
    my $sha_type = 256;
    my $d = Digest::SHA->new($sha_type);
    $d->addfile($file);

    return { sprintf('%s%d', 'sha', $sha_type) => $d->hexdigest };
} # calc_digest


sub calc_file_version {
    my $self          = shift;
    my $file          = shift;
    my $digest_record = shift;

    # FIXME : how do I know it's sha256 ?
    my $digest = $digest_record->{sha256};

    $file_version{$file}->{$digest} = 1;
    my @versions = keys %{ $file_version{$file} };
    my $v = $file_version{$file}->{$digest} = @versions;

    $self->log->debug("file_version:\n" . np(%file_version));

    return $v;
} # calc_file_version


sub get_versioned_file {
    my $self    = shift;
    my %params  = @_;
    my $file    = $params{file};
    my $version = $params{version};

    my ( $name, $path ) = fileparse($file);

    my $v_name = sprintf '%d.%s', $version, $name;
    my $v_dir  = File::Spec->catdir($self->dir, $path);
    my $v_file = File::Spec->catfile($v_dir, $v_name);

    return $v_file;
} # get_versioned_file


sub store_file {
    my $self = shift;
    my $old  = shift;
    my $new  = shift;

    my ( $name, $path, $suffix ) = fileparse($new);

    my $new_dir = File::Spec->catdir($self->dir, $path);
#    my $new_dir = $self->dir;
    make_path($new_dir);

    my $new_file = File::Spec->catfile($new_dir, $name);
    $self->log->debug("store_file: new_file => $new_file");

    my $digest = $self->calc_digest($old);
    $self->log->debug("store_file: digest:\n" . np($digest));
    my $version = $self->calc_file_version($new_file, $digest);
    $self->log->debug("store_file: version => $version");

    my $v_file = $self->get_versioned_file(
        file    => $new,
        version => $version,
    );

    if ( ! -e $v_file ) {
        $self->log->debug("File $v_file already exists.  Not storing.");

        rename $old, $v_file;
    }
    else {
        unlink $old;
    }

    return {
        digest         => $digest,
        file           => $new_file,
        versioned_path => $v_file,
        version        => $version,
    };
} # store_file


1;
