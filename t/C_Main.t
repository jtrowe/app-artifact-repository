use Mojo::Base -strict;

use Test::Deep;
use Test::More;
use Test::Mojo;

use App::Artifact::Repository::Server::Test;
use Data::Printer filters => [{
    'DateTime' => sub { return '' . $_[0]; },
}];
use IO::All;
use JSON;


plan(tests => 19);


my $re_json = qr/application\/json/i;

my $t = App::Artifact::Repository::Server::Test->new(
        'App::Artifact::Repository::Server');


$t->status_ok;

$t->list_repos_ok({
    repos    => [ qw( demo ) ],
});


my $file1 = 'poem.txt';
io($file1)->print('mary had a little lamb');
my $file1_v1_metadata = {
    date         => '2022-06-23T00:00:00+0000',
    meta_version => 3,
    tags         => [ qw(
        example_tag
    ) ],
};
$t->store_file_by_form_ok({
    artifact => $file1,
    file     => 'example/poem.txt',
    repo     => 'demo',
    metadata => $file1_v1_metadata,
    query    => {
        debug => 1,
    },
    response_extra_fields => {
        version => 1,
    },
});
my $file1_sha_v1 = $t->tx->res->json('/digest/sha256');


$t->store_file_by_form_missing_repo({
    file     => 'example/poem.txt',
    repo     => 'snafu',
    metadata => $file1_v1_metadata,
    query    => {
        debug => 1,
    },
    metadata => $file1_v1_metadata,
    query    => {
        debug => 1,
    },
});


io($file1)->append("it's fleece was white as snow");
$t->store_file_by_form_ok({
    artifact => $file1,
    file     => 'example/poem.txt',
    repo     => 'demo',
    metadata => {
        date         => '2022-06-23T00:00:00+0000',
        meta_version => 3,
        tags         => [],
    },
    query    => {
        debug => 1,
    },
    response_extra_fields => {
        version => 2,
    },
});
my $file1_sha_v2 = $t->tx->res->json('/digest/sha256');
isnt($file1_sha_v2, $file1_sha_v1, 'File digests are different');

my $expected_sto_index = {
    "example/poem.txt|1"                     => 'example/poem.txt|1',
    "example/poem.txt|1|example_tag"         => 'example/poem.txt|1',
    "example/poem.txt|example_tag"           => 'example/poem.txt|1',
    "example/poem.txt|{sha256}$file1_sha_v1" => 'example/poem.txt|1',
    "example/poem.txt|2"                     => 'example/poem.txt|2',
    "example/poem.txt"                       => 'example/poem.txt|2',
    "example/poem.txt|{sha256}$file1_sha_v2" => 'example/poem.txt|2',
};
my $meta_index = $t->app->aar->repos->{demo}->meta_index;
my @meta_keys  = keys %{ $meta_index };
my @expected_keys = (
    'example/poem.txt|1',
    'example/poem.txt|2',
);
cmp_bag(\@meta_keys, \@expected_keys,
        'Got correct expected keys in meta index');
cmp_deeply($t->app->aar->repos->{demo}->storage_index, $expected_sto_index,
        'Got expected storage index');


$t->fetch_artifact_by_digest_ok({
    digest      => $file1_sha_v1,
    digest_type => 'sha256',
    file        => 'example/poem.txt',
    repo        => 'demo',
    expected => {
        'Content-Type' => 'text/plain;charset=UTF-8',
    },
    query    => {
        debug => 1,
    },
});


$t->fetch_metadata_ok({
    file        => 'example/poem.txt',
    repo        => 'demo',
    expected => {
    },
    metadata => {
        date         => '2022-06-23T00:00:00+0000',
        meta_version => 3,
        tags         => [],
        version      => 2,
    },
    query    => {
        debug => 1,
    },
});


$t->fetch_metadata_missing_file({
    file        => 'example/poem.txt',
    repo        => 'demo',
    expected => {
    },
    metadata => {
        date         => '2022-06-23T00:00:00+0000',
        meta_version => 3,
        tags         => [],
        version      => 2,
    },
    query    => {
        debug => 1,
    },
});


$t->fetch_artifact_ok({
    file        => 'example/poem.txt',
    repo        => 'demo',
    expected => {
        'Content-Type' => 'text/plain;charset=UTF-8',
    },
    metadata => {
        date         => '2022-06-23T00:00:00+0000',
        meta_version => 3,
        tags         => [],
        version      => 2,
    },
    query    => {
        debug => 1,
    },
});


$t->fetch_artifact_missing_file({
    file        => 'example/never_uploaded.txt',
    repo        => 'demo',
    expected => {
        'Content-Type' => 'text/plain;charset=UTF-8',
    },
    metadata => {
        date         => '2022-06-23T00:00:00+0000',
        meta_version => 3,
        tags         => [],
        version      => 2,
    },
    query    => {
        debug => 1,
    },
});


# make sure version does not increment for a file already seen
$t->store_file_by_form_ok({
    artifact => $file1,
    file     => 'example/poem.txt',
    repo     => 'demo',
    metadata => {
        date         => '2022-06-23T00:00:00+0000',
        meta_version => 3,
        tags         => [],
    },
    query    => {
        debug => 1,
    },
    response_extra_fields => {
        version => 2,
    },
});
my $file1_sha_v2_again = $t->tx->res->json('/digest/sha256');
is($file1_sha_v2, $file1_sha_v2_again, 'File digests are the same');


# fetch a specific version
$t->fetch_metadata_ok({
    file        => 'example/poem.txt',
    repo        => 'demo',
    expected => {
    },
    metadata => {
        %{ $file1_v1_metadata },
        version => 1,
    },
    query    => {
        debug => 1,
        version => 1,
    },
});


$t->get_404({
    path => '/foo',
});


$t->fetch_index_ok({
    repo        => 'demo',
    query    => {
        debug => 1,
    },
});


$t->fetch_meta_index_ok({
    repo        => 'demo',
    query    => {
        debug => 1,
    },
});


